// ==UserScript==
// @name         Browser Rules plugin
// @namespace    https://*.maxymiser.com/
// @version      0.1
// @description  try to take over the world!
// @author       Igor Ievgeniev
// @match        https://*.maxymiser.com/CampaignBuilder*CampaignBrowserRules*
// @match        https://*.maxymiser.com/CampaignBuilder*DomainBrowserRules*
// @downloadURL  https://bitbucket.org/igorievgeniev/maxymiserplugins/raw/master/maxymiserBrowserRules.js
// @run-at       document-idle
// @grant        none
// ==/UserScript==

;(function(){
	'use strict';

	var BrowserRules = {

		/**
		 * The initialization state of the application
		 * @type {Boolean}
		 */
		isInitialized: false,

		/**
		 * Browser rules data to store
		 * @type {Object}
		 */
		browserRulesData: {
			BrowserId: {},
			OperatingSystemId: {},
			DeviceTypeId: {}
		},

		/**
		 * Attaches default events after page refreshing
		 */
		defaultEvents: function() {
			//has been taken from the default
			$(".mm-delete").click(function (e) {
				if (typeof $(this).data("is-postlink") != 'undefined' && !$(this).data("is-postlink")) return true;
				if ($(this).hasClass("disabled")) return false;
				var needConfirm = $(this).data("need-confirm");
				var confirmMessage = $(this).data("conf-message");

				if (typeof needConfirm != 'undefined' && !needConfirm) {
					PostIt(this);
					return false;
				}
				if (typeof confirmMessage != "undefined" && confirmMessage !== null && confirmMessage !== "")
					if (confirm(confirmMessage))
						PostIt(this);
					else
						e.stopImmediatePropagation();
				return false;
			});
		},

		renderMoreBrowserRules: function() {
			var self = this,
				url = window.location.origin + window.location.pathname + '?Grid-page=1&Grid-orderBy=~&Grid-filter=~&Grid-size=100';
			$.ajax({
				type: 'GET',
				url: url,
				success: function(response){
					var newRules = $(response).find('#Grid').html();
					$('#Grid').html(newRules);
					self.defaultEvents();
				}
			});
		},

		browserRulesSetUp: function(include, data) {
			var self = this,
				queue = data.length,
				progress = 0;

			var _setialize = function(obj){
				var str = '';
				for (var key in obj) { str = str + key+'='+obj[key]+'&'; }
				return str;
			};

			var ruleType = 'ExcludeRule=Exclude&';
			var $form = $('#campaignBrowserRules form, #domainBrowserRules form'),
				url = $form.find('.primary-button.primary-green-button').attr('data-post-url'),
				token = 'XsrfToken='+$form.find('#xsrfToken').val();

			if (include) ruleType = 'IncludeRule=Include&';

			if (!include) $('.br_progress').addClass('br_progress_exclude');
			$('.br_progress > i').text('0%');
			$('.br_progress > span').css('width', '0%');
			setTimeout(function(){ $('.br_progress').show(); }, 500);


			for (var i = 0; i < data.length; i++) {
				var formData = _setialize(data[i]) + ruleType + token;
				$.ajax({
					type: 'POST',
					url: url,
					data: formData,
					success: function(response){
						var newRules = $(response).find('#Grid').html();
						$('#Grid').html(newRules);

						progress = 100/data.length + progress;
						$('.br_progress > span').css('width', progress+'%');
						$('.br_progress > i').text(Math.round(progress)+'%');
						queue--;

						if (queue === 0) {
							self.renderMoreBrowserRules();
							setTimeout(function(){ $('.br_progress').hide(); }, 1000);
							setTimeout(function(){ $('.mm_br_overlay').fadeOut(); }, 1500);
						}
					}
				});
			}
		},

		/**
		 * Renders browser rules UI
		 */
		browserRulesUI: function() {
			var uiTpl = '<style type="text/css">'+
			'	.br_add {'+
			'		margin: 10px 0 50px;'+
			'	}'+
			'	.br_add:hover {'+
			'		color: #fff;'+
			'	}'+
			'	.mm_br_overlay {'+
			'		position: fixed;'+
			'		top: 0;'+
			'		left: 0;'+
			'		z-index: 1000;'+
			'		width: 100%;'+
			'		height: 100%;'+
			'		background-color: rgba(0, 0, 0, .4);'+
			'		cursor: pointer;'+
			'		display: none;'+
			'	}'+
			'	.mm_br_popup {'+
			'		width: 680px;'+
			'		position: fixed;'+
			'		top: 50%;'+
			'		left: 50%;'+
			'		margin: -100px 0 0 -340px;'+
			'		z-index: 999;'+
			'		background: #f1f1f1;'+
			'		padding: 10px;'+
			'		box-sizing: border-box;'+
			'		font-size: 14px;'+
			'		border-radius: 4px;'+
			'		border: 1px solid #ccc;'+
			'		cursor: default;'+
			'	}'+
			'	.mm_br_popup * {'+
			'		box-sizing: border-box;'+
			'		user-select: none;'+
			'		-moz-user-select: none;'+
			'		-webkit-user-select: none;'+
			'	}'+
			'	.mm_br_popup label {'+
			'		cursor: pointer;'+
			'	}'+
			'	.mm_br_popup .mm_br_wrapper {'+
			'		'+
			'	}'+
			'	.mm_br_popup .mm_br_data {'+
			'		overflow: hidden;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type {'+
			'		width: 33%;'+
			'		float: left;'+
			'		border: 1px solid #969696;'+
			'		margin-right: 2.5%;'+
			'		border-radius: 4px;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type:last-child {'+
			'		margin-right: 0;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type.mm_br_desktop {'+
			'		width: 40%;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type.mm_br_tablet,'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type.mm_br_mobile {'+
			'		width: 27.5%;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type > h5 {'+
			'		text-align: center;'+
			'		cursor: pointer;'+
			'		background: #ccc;'+
			'		padding: 5px;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type > .mm_br_type {'+
			'		overflow: hidden;'+
			'		padding: 10px 5px;'+
			'		border-top: 1px solid #969696;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type > .mm_br_type span {'+
			'		float: left;'+
			'		width: 60%;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type > .mm_br_type span:first-child {'+
			'		width: 30%;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type > .mm_br_type span:last-child {'+
			'		float: right;'+
			'		width: 8%;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type > .mm_br_type span > input[type="text"] {'+
			'		height: 15px;'+
			'		display: block;'+
			'		width: 100%;'+
			'		padding: 0 5px;'+
			'		border: 1px solid #969696;'+
			'		border-radius: 4px;'+
			'		height: 20px;'+
			'		outline: none;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type > .mm_br_type span > input[type="checkbox"] {'+
			'		margin: 0;'+
			'		float: right;'+
			'		outline: none;'+
			'		cursor: pointer;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type .mm_br_oc_type {'+
			'		overflow: hidden;'+
			'		border-top: 1px solid #969696;'+
			'		padding: 5px;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type .mm_br_oc_type .mm_br_os_name {'+
			'		width: 40%;'+
			'		float: left;'+
			'		padding: 5px 0;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type .mm_br_oc_type .mm_br_os_name span {'+
			'		cursor: pointer;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type .mm_br_oc_type .mm_br_type {'+
			'		width: 60%;'+
			'		float: right;'+
			'		overflow: hidden;'+
			'		padding: 5px 0;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type .mm_br_oc_type .mm_br_type span {'+
			'		float: right;'+
			'	}'+
			'	.mm_br_popup .mm_br_data .mm_br_device_type .mm_br_oc_type .mm_br_type span > input[type="checkbox"] {'+
			'		margin: 0 0 0 10px;'+
			'		float: right;'+
			'		outline: none;'+
			'		cursor: pointer;'+
			'	}'+
			'	.mm_br_popup .mm_br_buttons {'+
			'		margin: 20px auto 0;'+
			'		text-align: right;'+
			'	}'+
			'	.mm_br_popup .mm_br_buttons > span {'+
			'		display: inline-block;'+
			'		padding: 5px 34px;'+
			'		border-radius: 4px;'+
			'		color: #fff;'+
			'		cursor: pointer;'+
			'		margin-left: 10px;'+
			'	}'+
			'	.mm_br_popup .mm_br_buttons .mm_br_button_include {'+
			'		border: 1px solid #006600;'+
			'		background: #009900;'+
			'		background: -moz-linear-gradient(top, #009900 0%, #007d00 100%);'+
			'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #009900), color-stop(100%, #007d00));'+
			'		background: -webkit-linear-gradient(top, #009900 0%, #007d00 100%);'+
			'		background: -o-linear-gradient(top, #009900 0%, #007d00 100%);'+
			'		background: -ms-linear-gradient(top, #009900 0%, #007d00 100%);'+
			'		background: linear-gradient(to bottom, #009900 0%, #007d00 100%);'+
			'	}'+
			'	.mm_br_popup .mm_br_buttons .mm_br_button_include:active {'+
			'		background: #007d00;'+
			'		background: -moz-linear-gradient(top, #007d00 0%, #009900 100%);'+
			'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #007d00), color-stop(100%, #009900));'+
			'		background: -webkit-linear-gradient(top, #007d00 0%, #009900 100%);'+
			'		background: -o-linear-gradient(top, #007d00 0%, #009900 100%);'+
			'		background: -ms-linear-gradient(top, #007d00 0%, #009900 100%);'+
			'		background: linear-gradient(to bottom, #007d00 0%, #009900 100%);'+
			'	}'+
			'	.mm_br_popup .mm_br_buttons .mm_br_button_exclude {'+
			'		border: 1px solid #990000;'+
			'		background: #e74545;'+
			'		background: -moz-linear-gradient(top, #e74545 0%, #c83934 100%);'+
			'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #e74545), color-stop(100%, #c83934));'+
			'		background: -webkit-linear-gradient(top, #e74545 0%, #c83934 100%);'+
			'		background: -o-linear-gradient(top, #e74545 0%, #c83934 100%);'+
			'		background: -ms-linear-gradient(top, #e74545 0%, #c83934 100%);'+
			'		background: linear-gradient(to bottom, #e74545 0%, #c83934 100%);'+
			'	}'+
			'	.mm_br_popup .mm_br_buttons .mm_br_button_exclude:active {'+
			'		background: #c83934;'+
			'		background: -moz-linear-gradient(top, #c83934 0%, #e74545 100%);'+
			'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #c83934), color-stop(100%, #e74545));'+
			'		background: -webkit-linear-gradient(top, #c83934 0%, #e74545 100%);'+
			'		background: -o-linear-gradient(top, #c83934 0%, #e74545 100%);'+
			'		background: -ms-linear-gradient(top, #c83934 0%, #e74545 100%);'+
			'		background: linear-gradient(to bottom, #c83934 0%, #e74545 100%);'+
			'	}'+
			'	.br_progress {'+
			'		width: 50%;'+
			'		float: right;'+
			'		border: none;'+
			'		height: 15px;'+
			'		padding: 0;'+
			'		background-color: #eee;'+
			'		border-radius: 2px;'+
			'		position: relative;'+
			'		margin-top: -20px;'+
			'		display: none;'+
			'		max-width: 250px;'+
			'		box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25) inset;'+
			'	}'+
			'	.br_progress > i {'+
			'		position: absolute;'+
			'		width: 100%;'+
			'		left: 0;'+
			'		top: 0;'+
			'		font-size: 14px;'+
			'		line-height: 15px;'+
			'		text-align: center;'+
			'	}'+
			'	.br_progress > span {'+
			'		border-radius: 2px;'+
			'		display: block;'+
			'		height: 15px;'+
			'		width: 0%;'+
			'		transition: width .5s linear 0s;'+
			'		background-color: rgb(43,194,83);'+
			'		background-image: linear-gradient(center bottom, rgb(43,194,83) 37%, rgb(84,240,84) 69%);'+
			'		background-color: rgb(43,194,83);'+
			'		background-image: -webkit-gradient( linear, left bottom, left top, color-stop(0, rgb(43,194,83)), color-stop(1, rgb(84,240,84)) );'+
			'		background-image: -moz-linear-gradient( center bottom, rgb(43,194,83) 37%, rgb(84,240,84) 69% );'+
			'		box-shadow: inset 0 2px 9px  rgba(255,255,255,0.3), inset 0 -2px 6px rgba(0,0,0,0.4);'+
			'		position: relative;'+
			'		overflow: hidden;'+
			'	}'+
			'	.br_progress.br_progress_exclude > span {'+
			'		background-color: #f0a3a3;'+
			'		background-image: -moz-linear-gradient(top, #f0a3a3, #f42323);'+
			'		background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f0a3a3),color-stop(1, #f42323));'+
			'		background-image: -webkit-linear-gradient(#f0a3a3, #f42323);'+
			'	}'+
			'	.br_progress > span:after {'+
			'		content: "";'+
			'		position: absolute;'+
			'		top: 0; left: 0; bottom: 0; right: 0;'+
			'		background-image: linear-gradient(-45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);'+
			'		z-index: 1;'+
			'		background-size: 50px 50px;'+
			'		animation: move 2s linear infinite;'+
			'		border-top-right-radius: 8px;'+
			'		border-bottom-right-radius: 8px;'+
			'		border-top-left-radius: 20px;'+
			'		border-bottom-left-radius: 20px;'+
			'		overflow: hidden;'+
			'	}'+
			'	@-webkit-keyframes move {'+
			'		0% {'+
			'			background-position: 0 0;'+
			'		}'+
			'		100% {'+
			'			background-position: 50px 50px;'+
			'		}'+
			'	}'+
			'	@-moz-keyframes move {'+
			'		0% {'+
			'			background-position: 0 0;'+
			'		}'+
			'		100% {'+
			'			background-position: 50px 50px;'+
			'		}'+
			'	}'+
			'</style>'+
			'<div class="mm_br_overlay">'+
			'	<div class="mm_br_popup">'+
			'		<h2>Manage Browser Rules</h2>'+
			'		<div class="mm_br_wrapper">'+
			'			<div class="mm_br_data">'+
			'				<div class="mm_br_device_type mm_br_desktop">'+
			'					<h5>Desktop</h5>'+
			'					<div class="mm_br_type mm_br_type_ie">'+
			'						<span><label for="br_desktop_ie">IE</label></span>'+
			'						<span><input type="text" name="versions" value="11.*"></span>'+
			'						<span><input id="br_desktop_ie" type="checkbox" checked data-device="Desktop" data-browser="IE" data-os="Any" name="ie" value="1"></span>'+
			'					</div>'+
			'					<div class="mm_br_type mm_br_type_edge">'+
			'						<span><label for="br_desktop_edge">Edge</label></span>'+
			'						<span></span>'+
			'						<span><input id="br_desktop_edge" type="checkbox" checked data-device="Desktop" data-browser="Edge" data-os="Any" name="edge" value="1365"></span>'+
			'					</div>'+
			'					<div class="mm_br_type mm_br_type_safari">'+
			'						<span><label for="br_desktop_safari">Safari</label></span>'+
			'						<span><input type="text" name="" value="10.*"></span>'+
			'						<span><input id="br_desktop_safari" type="checkbox" checked data-device="Desktop" data-browser="Safari" data-os="Any" name="safari" value="3"></span>'+
			'					</div>'+
			'					<div class="mm_br_type mm_br_type_chrome">'+
			'						<span><label for="br_desktop_chrome">Chrome</label></span>'+
			'						<span></span>'+
			'						<span><input id="br_desktop_chrome" type="checkbox" checked data-device="Desktop" data-browser="Chrome" data-os="Any" name="chrome" value="47"></span>'+
			'					</div>'+
			'					<div class="mm_br_type mm_br_type_firefox">'+
			'						<span><label for="br_desktop_firefox">Firefox</label></span>'+
			'						<span><input type="text" name="" value="5*"></span>'+
			'						<span><input id="br_desktop_firefox" type="checkbox" checked data-device="Desktop" data-browser="Firefox" data-os="Any" name="firefox" value="2"></span>'+
			'					</div>'+
			'				</div>'+
			'				<div class="mm_br_device_type mm_br_tablet">'+
			'					<h5>Tablet</h5>'+
			'					<div class="mm_br_os">'+
			'						<div class="mm_br_oc_type mm_br_oc_type_ios">'+
			'							<div class="mm_br_os_name"><span>iOS</span></div>'+
			'							<div class="mm_br_type mm_br_type_chrome">'+
			'								<span><input id="br_tablet_ios_chrome" type="checkbox" checked data-device="Tablet" data-browser="Chrome" data-os="iPhone OS" name="chrome" value="47"></span>'+
			'								<span><label for="br_tablet_ios_chrome">Chrome</label></span>'+
			'							</div>'+
			'							<div class="mm_br_type mm_br_type_safari">'+
			'								<span><input id="br_tablet_ios_safari" type="checkbox" checked data-device="Tablet" data-browser="Safari" data-os="iPhone OS" name="safari" value="3"></span>'+
			'								<span><label for="br_tablet_ios_safari">Safari</label></span>'+
			'							</div>'+
			'						</div>'+
			'						<div class="mm_br_oc_type mm_br_oc_type_android">'+
			'							<div class="mm_br_os_name"><span>Android</span></div>'+
			'							<div class="mm_br_type mm_br_type_chrome">'+
			'								<span><input id="br_tablet_android_chrome" type="checkbox" checked data-device="Tablet" data-browser="Chrome" data-os="Android" name="chrome" value="47"></span>'+
			'								<span><label for="br_tablet_android_chrome">Chrome</label></span>'+
			'							</div>'+
			'							<div class="mm_br_type mm_br_type_webkit">'+
			'								<span><input id="br_tablet_android_webkit" type="checkbox" checked data-device="Tablet" data-browser="Android Webkit" data-os="Android" name="webkit" value="1354"></span>'+
			'								<span><label for="br_tablet_android_webkit">Webkit</label></span>'+
			'							</div>'+
			'						</div>'+
			'					</div>'+
			'				</div>'+
			'				<div class="mm_br_device_type mm_br_mobile">'+
			'					<h5>Mobile</h5>'+
			'					<div class="mm_br_os">'+
			'						<div class="mm_br_oc_type mm_br_oc_type_ios">'+
			'							<div class="mm_br_os_name"><span>iOS</span></div>'+
			'							<div class="mm_br_type mm_br_type_chrome">'+
			'								<span><input id="br_phone_ios_chrome" type="checkbox" checked data-device="Phone" data-browser="Chrome" data-os="iPhone OS" name="chrome" value="47"></span>'+
			'								<span><label for="br_phone_ios_chrome">Chrome</label></span>'+
			'							</div>'+
			'							<div class="mm_br_type mm_br_type_safari">'+
			'								<span><input id="br_phone_ios_safari" type="checkbox" checked data-device="Phone" data-browser="Safari" data-os="iPhone OS" name="safari" value="3"></span>'+
			'								<span><label for="br_phone_ios_safari">Safari</label></span>'+
			'							</div>'+
			'						</div>'+
			'						<div class="mm_br_oc_type mm_br_oc_type_android">'+
			'							<div class="mm_br_os_name"><span>Android</span></div>'+
			'							<div class="mm_br_type mm_br_type_chrome">'+
			'								<span><input id="br_phone_android_chrome" type="checkbox" checked data-device="Phone" data-browser="Chrome" data-os="Android" name="chrome" value="47"></span>'+
			'								<span><label for="br_phone_android_chrome">Chrome</label></span>'+
			'							</div>'+
			'							<div class="mm_br_type mm_br_type_webkit">'+
			'								<span><input id="br_phone_android_webkit" type="checkbox" checked data-device="Phone" data-browser="Android Webkit" data-os="Android" name="webkit" value="1354"></span>'+
			'								<span><label for="br_phone_android_webkit">Webkit</label></span>'+
			'							</div>'+
			'						</div>'+
			'					</div>'+
			'				</div>'+
			'			</div>'+
			'			<div class="br_progress"><span></span><i></i></div>'+
			'			<div class="mm_br_buttons">'+
			'				<span class="mm_br_button_include">Include</span>'+
			'				<span class="mm_br_button_exclude">Exclude</span>'+
			'			</div>'+
			'		</div>'+
			'	</div>'+
			'</div>',
			button = '<div class="br_holder"><a class="br_add primary-green-button">Add the most common browser rules</a></div>';

			$('#campaignBrowserRules, #domainBrowserRules').after(button);
			$('body').append(uiTpl);

			$('.br_add').click(function(){ $('.mm_br_overlay').fadeIn(); });
			$('.mm_br_overlay').click(function(e){ if ($(e.target).is('.mm_br_overlay')) $('.mm_br_overlay').fadeOut(); });
		},

		/**
		 * Stores browser rules data to the browserRulesData object
		 */
		storeData: function() {
			var self = this;
			$('select#BrowserId, select#OperatingSystemId, select#DeviceTypeId').each(function(){
				var id = $(this).attr('id');
				$(this).find('option').each(function(){ self.browserRulesData[id][$(this).text()] = $(this).val(); });
			});
		},

		browserRulesAttachEvents: function(data) {
			var self = this;

			// include/exclude events
			$('.mm_br_buttons > span').click(function(e){
				var isInclude = $(this).is('.mm_br_button_include'),
					rules = [];
				if(!$('.mm_br_data input[type="checkbox"]:checked').length) return;
				$('.mm_br_data input[type="checkbox"]:checked').each(function(){
					var deviceType = data.DeviceTypeId[$(this).attr('data-device')],
						browserId = data.BrowserId[$(this).attr('data-browser')],
						osId = data.OperatingSystemId[$(this).attr('data-os')],
						browserVersionMask = '';

					var mask = $(this).closest('.mm_br_type').find('input[type="text"]').val(),
						masks = [];

					if (mask) {
						masks = mask.split(/\,\s*/);
						for (var i = 0; i < masks.length; i++) {
							rules.push({
								BrowserId : browserId,
								BrowserVersionMask : masks[i],
								OperatingSystemId : osId,
								DeviceTypeId : deviceType
							});
						}
					} else {
						rules.push({
							BrowserId : browserId,
							BrowserVersionMask : browserVersionMask,
							OperatingSystemId : osId,
							DeviceTypeId : deviceType
						});
					}
				});

				self.browserRulesSetUp(isInclude, rules);
			});

			//checkboxes selection
			$('.mm_br_device_type h5').toggle(function(){
				$(this).closest('.mm_br_device_type').find('input[type="checkbox"]').removeAttr('checked');
			}, function(){
				$(this).closest('.mm_br_device_type').find('input[type="checkbox"]').attr('checked', 'checked');
			});

			$('.mm_br_os_name span').toggle(function(){
				$(this).closest('.mm_br_oc_type').find('input[type="checkbox"]').removeAttr('checked');
			}, function(){
				$(this).closest('.mm_br_oc_type').find('input[type="checkbox"]').attr('checked', 'checked');
			});
		},

		init: function() {
			if(BrowserRules.isInitialized) return;

			BrowserRules.storeData();
			BrowserRules.browserRulesUI();
			BrowserRules.browserRulesAttachEvents(BrowserRules.browserRulesData);
			BrowserRules.isInitialized = true;
		}

	};

	//init
	BrowserRules.init();

})();