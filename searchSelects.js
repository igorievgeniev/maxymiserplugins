// ==UserScript==
// @name         Search Select
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  To include search fields to the selects
// @author       Me
// @require      https://code.jquery.com/jquery-2.1.4.min.js
// @match        http*://rsm.us.oracle.com/rsm/alms.jsp*
// @run-at       document-idle
// @downloadURL  https://bitbucket.org/igorievgeniev/maxymiserplugins/raw/master/searchSelects.js
// @grant        none
// ==/UserScript==

;(function() {
	'use strict';

	function addFilterInput() {
		$('select').each(function () {
			var $input = $('<input type="text" class="filter-options" placeholder="Filter" title="RegExp">'),
				$select = $(this),
				options = [];
			$input.insertBefore($select);
			$select.find('option').each(function () {
				options.push({value: this.value, text: this.text});
			});
			$select.data('options', options);
			//keyup
			$input.bind('change', function () {
				var t0 = performance.now();
				var options, regex, output = '';
				options = $select.empty().data('options');
				var t1 = performance.now();
				try {
					regex = new RegExp($(this).val(), 'gi');
					for (var i = 0, len = options.length; i < len; i++) {
						var option = options[i];
						if (regex.test(option.text)) output += '<option value="'+option.value+'"">'+option.text+'</option>';
					}
					$select.html(output);
				} catch (error) {}
				console.log("Call to doSomething took " + (t1 - t0) + " milliseconds.")
			});
		});
	}

	addFilterInput();

})();