// ==UserScript==
// @name         page masks plugin
// @namespace    https://*.maxymiser.com/
// @version      0.2
// @description  try to take over the world!
// @author       Igor Ievgeniev
// @match        https://*.maxymiser.com/CampaignBuilder*DomainLocations*
// @downloadURL  https://bitbucket.org/igorievgeniev/maxymiserplugins/raw/master/maxymiserPageMasks.js
// @run-at       document-idle
// @grant        none
// ==/UserScript==

;(function(){
	'use strict';

	var pageMasks = {

		/**
		 * The initialization state of the application
		 * @type {Boolean}
		 */
		isInitialized: false,

		renderUI: function() {
			var uiTpl = '<style type="text/css">'+
						'	.mm_pm_overlay {'+
						'		position: fixed;'+
						'		top: 0;'+
						'		left: 0;'+
						'		z-index: 1000;'+
						'		width: 100%;'+
						'		height: 100%;'+
						'		background-color: rgba(0, 0, 0, .4);'+
						'		cursor: pointer;'+
						'		display: none;'+
						'	}'+
						'	.mm_pm_popup {'+
						'		width: 800px;'+
						'		position: fixed;'+
						'		top: 50%;'+
						'		left: 50%;'+
						'		transform: translate(-50%, -50%);'+
						'		z-index: 999;'+
						'		background: #f1f1f1;'+
						'		padding: 10px;'+
						'		box-sizing: border-box;'+
						'		font-size: 14px;'+
						'		border-radius: 4px;'+
						'		border: 1px solid #ccc;'+
						'		cursor: default;'+
						'	}'+
						'	.mm_pm_popup * {'+
						'		box-sizing: border-box;'+
						'		user-select: none;'+
						'		-moz-user-select: none;'+
						'		-webkit-user-select: none;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner {'+
						'		overflow: hidden;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div {'+
						'		float: left;'+
						'		width: 30%;'+
						'		border-radius: 4px;'+
						'		border: 1px solid #969696;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div:last-child {'+
						'		margin-right: 0;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div.mm_pm_urls {'+
						'		width: 69%;'+
						'		margin-right: 1%;'+
						'		background: #fff;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div > h5 {'+
						'		text-align: center;'+
						'		background: #ccc;'+
						'		padding: 5px;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div textarea {'+
						'		resize: none;'+
						'		border: none;'+
						'		min-height: 200px;'+
						'		height: auto;'+
						'		max-height: 400px;'+
						'		padding: 0;'+
						'		margin: 0;'+
						'		outline: none;'+
						'		padding: 5px;'+
						'		font-size: 12px;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div.mm_pm_replace {'+
						'		position: relative;'+
						'		margin-top: 15px;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div .mm_pm_group {'+
						'		padding: 4px;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div.mm_pm_replace .mm_pm_group input {'+
						'		padding: 0 4px;'+
						'		border: 1px solid #969696;'+
						'		width: 90px;'+
						'		line-height: 18px;'+
						'		font-size: 14px;'+
						'	}'+
						'	.mm_pm_remove_group {'+
						'		position: relative;'+
						'		float: right;'+
						'		cursor: pointer;'+
						'		width: 20px;'+
						'		height: 20px;'+
						'		opacity: 0.3;'+
						'	}'+
						'	.mm_pm_remove_group:hover {'+
						'		opacity: 1;'+
						'	}'+
						'	.mm_pm_remove_group:before,'+
						'	.mm_pm_remove_group:after {'+
						'		position: absolute;'+
						'		left: 10px;'+
						'		content: " ";'+
						'		height: 20px;'+
						'		width: 2px;'+
						'		background-color: #333;'+
						'	}'+
						'	.mm_pm_remove_group:before {'+
						'		transform: rotate(45deg);'+
						'	}'+
						'	.mm_pm_remove_group:after {'+
						'		transform: rotate(-45deg);'+
						'		top: -1px;'+
						'	}'+
						'	.mm_pm_add_group {'+
						'		position: absolute;'+
						'		top: 3px;'+
						'		right: 3px;'+
						'		cursor: pointer;'+
						'		width: 18px;'+
						'		height: 18px;'+
						'		border-radius: 4px;'+
						'		background: #fff;'+
						'	}'+
						'	.mm_pm_add_group:hover {'+
						'		background: #5f5f5f;'+
						'	}'+
						'	.mm_pm_add_group:before,'+
						'	.mm_pm_add_group:after {'+
						'		position: absolute;'+
						'		top: 2px;'+
						'		left: 8px;'+
						'		content: " ";'+
						'		height: 14px;'+
						'		width: 2px;'+
						'		background-color: #a2a2a2;'+
						'	}'+
						'	.mm_pm_add_group:before {'+
						'		transform: rotate(90deg);'+
						'		top: 3px;'+
						'		left: 7px;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_popup_inner > div.mm_pm_ending .mm_pm_group input {'+
						'		padding: 0 4px;'+
						'		border: 1px solid #969696;'+
						'		width: 100%;'+
						'		line-height: 18px;'+
						'		font-size: 14px;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_buttons {'+
						'		margin: 50px auto 0;'+
						'		text-align: right;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_buttons > span {'+
						'		display: inline-block;'+
						'		padding: 5px 34px;'+
						'		border-radius: 4px;'+
						'		color: #fff;'+
						'		cursor: pointer;'+
						'		margin-left: 10px;'+
						'	}'+
						'	.mm_pm_popup .mm_pm_buttons .mm_pm_button_exclude {'+
						'		border: 1px solid #990000;'+
						'		background: #e74545;'+
						'		background: -moz-linear-gradient(top, #e74545 0%, #c83934 100%);'+
						'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #e74545), color-stop(100%, #c83934));'+
						'		background: -webkit-linear-gradient(top, #e74545 0%, #c83934 100%);'+
						'		background: -o-linear-gradient(top, #e74545 0%, #c83934 100%);'+
						'		background: -ms-linear-gradient(top, #e74545 0%, #c83934 100%);'+
						'		background: linear-gradient(to bottom, #e74545 0%, #c83934 100%);'+
						'	}'+
						'	.mm_pm_popup .mm_pm_buttons .mm_pm_button_include {'+
						'		border: 1px solid #006600;'+
						'		background: #009900;'+
						'		background: -moz-linear-gradient(top, #009900 0%, #007d00 100%);'+
						'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #009900), color-stop(100%, #007d00));'+
						'		background: -webkit-linear-gradient(top, #009900 0%, #007d00 100%);'+
						'		background: -o-linear-gradient(top, #009900 0%, #007d00 100%);'+
						'		background: -ms-linear-gradient(top, #009900 0%, #007d00 100%);'+
						'		background: linear-gradient(to bottom, #009900 0%, #007d00 100%);'+
						'	}'+
						'	.mm_pm_popup .mm_pm_buttons .mm_pm_button_include:active {'+
						'		background: #007d00;'+
						'		background: -moz-linear-gradient(top, #007d00 0%, #009900 100%);'+
						'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #007d00), color-stop(100%, #009900));'+
						'		background: -webkit-linear-gradient(top, #007d00 0%, #009900 100%);'+
						'		background: -o-linear-gradient(top, #007d00 0%, #009900 100%);'+
						'		background: -ms-linear-gradient(top, #007d00 0%, #009900 100%);'+
						'		background: linear-gradient(to bottom, #007d00 0%, #009900 100%);'+
						'	}'+
						'	.mm_pm_popup .mm_pm_buttons .mm_pm_button_exclude:active {'+
						'		background: #c83934;'+
						'		background: -moz-linear-gradient(top, #c83934 0%, #e74545 100%);'+
						'		background: -webkit-gradient(left top, left bottom, color-stop(0%, #c83934), color-stop(100%, #e74545));'+
						'		background: -webkit-linear-gradient(top, #c83934 0%, #e74545 100%);'+
						'		background: -o-linear-gradient(top, #c83934 0%, #e74545 100%);'+
						'		background: -ms-linear-gradient(top, #c83934 0%, #e74545 100%);'+
						'		background: linear-gradient(to bottom, #c83934 0%, #e74545 100%);'+
						'	}'+
						'	.mm_pm_progress {'+
						'		width: 50%;'+
						'		float: right;'+
						'		border: none;'+
						'		height: 15px;'+
						'		padding: 0;'+
						'		background-color: #eee;'+
						'		border-radius: 2px;'+
						'		position: relative;'+
						'		margin-top: 15px;'+
						'		display: none;'+
						'		max-width: 250px;'+
						'		box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25) inset;'+
						'	}'+
						'	.mm_pm_progress > span {'+
						'		border-radius: 2px;'+
						'		display: block;'+
						'		height: 15px;'+
						'		width: 0%;'+
						'		transition: width .5s linear 0s;'+
						'		background-color: rgb(43,194,83);'+
						'		background-image: linear-gradient(center bottom, rgb(43,194,83) 37%, rgb(84,240,84) 69%);'+
						'		background-color: rgb(43,194,83);'+
						'		background-image: -webkit-gradient( linear, left bottom, left top, color-stop(0, rgb(43,194,83)), color-stop(1, rgb(84,240,84)) );'+
						'		background-image: -moz-linear-gradient( center bottom, rgb(43,194,83) 37%, rgb(84,240,84) 69% );'+
						'		box-shadow: inset 0 2px 9px rgba(255,255,255,0.3), inset 0 -2px 6px rgba(0,0,0,0.4);'+
						'		position: relative;'+
						'		overflow: hidden;'+
						'	}'+
						'	.mm_pm_progress > i {'+
						'		position: absolute;'+
						'		width: 100%;'+
						'		left: 0;'+
						'		top: 0;'+
						'		font-size: 14px;'+
						'		line-height: 15px;'+
						'		text-align: center;'+
						'	}'+
						'	.mm_pm_progress.mm_pm_progress_exclude > span {'+
						'		background-color: #f0a3a3;'+
						'		background-image: -moz-linear-gradient(top, #f0a3a3, #f42323);'+
						'		background-image: -webkit-gradient(linear,left top,left bottom,color-stop(0, #f0a3a3),color-stop(1, #f42323));'+
						'		background-image: -webkit-linear-gradient(#f0a3a3, #f42323);'+
						'	}'+
						'	.mm_pm_progress > span:after {'+
						'		content: "";'+
						'		position: absolute;'+
						'		top: 0; left: 0; bottom: 0; right: 0;'+
						'		background-image: linear-gradient(-45deg, rgba(255, 255, 255, .2) 25%, transparent 25%, transparent 50%, rgba(255, 255, 255, .2) 50%, rgba(255, 255, 255, .2) 75%, transparent 75%, transparent);'+
						'		z-index: 1;'+
						'		background-size: 50px 50px;'+
						'		animation: move 2s linear infinite;'+
						'		border-top-right-radius: 8px;'+
						'		border-bottom-right-radius: 8px;'+
						'		border-top-left-radius: 20px;'+
						'		border-bottom-left-radius: 20px;'+
						'		overflow: hidden;'+
						'	}'+
						'	.mm_pm_holder {'+
						'		padding: 10px 0 30px;'+
						'	}'+
						'	.mm_pm_holder a:hover {'+
						'		color: #333;'+
						'	}'+
						'	@-webkit-keyframes move {'+
						'		0% {'+
						'			background-position: 0 0;'+
						'		}'+
						'		100% {'+
						'			background-position: 50px 50px;'+
						'		}'+
						'	}'+
						'	@-moz-keyframes move {'+
						'		0% {'+
						'			background-position: 0 0;'+
						'		}'+
						'		100% {'+
						'			background-position: 50px 50px;'+
						'		}'+
						'	}'+
						'</style>'+
						'<div class="mm_pm_overlay">'+
						'	<div class="mm_pm_popup">'+
						'		<h2>Manage Page Masks</h2>'+
						'		<div class="mm_pm_popup_inner">'+
						'			<div class="mm_pm_urls">'+
						'				<h5>URLs:</h5>'+
						'				<textarea placeholder="https://www.domain.com, https://www.subdomain.domain.com" data-field="mm_pm_urls"></textarea>'+
						'			</div>'+
						'			<div class="mm_pm_ending">'+
						'				<h5>Ending:</h5>'+
						'				<div class="mm_pm_group">'+
						'					<input data-field="ending" type="text" placeholder="/, ?*, /?*, #*, /#*" value="/, ?*, /?*, #*, /#*">'+
						'				</div>'+
						'			</div>'+
						'			<div class="mm_pm_replace">'+
						'				<h5>Replace:</h5>'+
						'				<span class="mm_pm_add_group" title="Add group"></span>'+
						'				<div class="mm_pm_group_list">'+
						'					<div class="mm_pm_group">'+
						'						<input data-field="replace-from" type="text" placeholder="from" value="https"><span> = </span><input data-field="replace-to" type="text" placeholder="to" value="http*"><span class="mm_pm_remove_group" title="Remove group"></span>'+
						'					</div>'+
						'					<div class="mm_pm_group">'+
						'						<input data-field="replace-from" type="text" placeholder="from" value="www."><span> = </span><input data-field="replace-to" type="text" placeholder="to" value="*"><span class="mm_pm_remove_group" title="Remove group"></span>'+
						'					</div>'+
						'				</div>'+
						'			</div>'+
						'		</div>'+
						'		<div class="mm_pm_progress"><span></span><i></i></div>'+
						'		<div class="mm_pm_buttons">'+
						'			<span class="mm_pm_button_include">Include</span>'+
						'			<span class="mm_pm_button_exclude">Exclude</span>'+
						'		</div>'+
						'	</div>'+
						'</div>';
			var button = '<div class="mm_pm_holder"><a class="mm_pm_manage secondary-button">Manage Page Masks</a></div>';

			$('#isOverlayDependentArea > h2').after(button);
			$('body').append(uiTpl);

			$('.mm_pm_manage').click(function(){ $('.mm_pm_overlay').fadeIn(); });
			$('.mm_pm_overlay').click(function(e){ if ($(e.target).is('.mm_pm_overlay')) $('.mm_pm_overlay').fadeOut(); });

			$(document).delegate('.mm_pm_add_group', 'click', function(){
				var groupTpl = '<div class="mm_pm_group">'+
									'<input data-field="replace-from" type="text" placeholder="from" value="">'+
									'<span> = </span>'+
									'<input data-field="replace-to" type="text" placeholder="to" value="">'+
									'<span class="mm_pm_remove_group" title="Remove group"></span>'+
								'</div>';
				$(this).closest('div').find('.mm_pm_group_list').prepend(groupTpl);
			});

			$(document).delegate('.mm_pm_remove_group', 'click', function(){
				$(this).closest('.mm_pm_group').remove();
			});
		},

		attachHandlers: function(){
			var self = this;

			$('.mm_pm_button_include').click(function(){
				var data = self.fetchData(),
					arr = self.prepareUrl(data.pages, data.replace, data.ending);
				self.addMask(arr);
			});

			$('.mm_pm_button_exclude').click(function(){
				var data = self.fetchData(),
					arr = self.prepareUrl(data.pages, data.replace, data.ending);
				self.addMask(arr, 'exclude');
			});
		},

		fetchData: function() {
			var pages = $('.mm_pm_urls [data-field="mm_pm_urls"]').val().split(/[\n\s\,]+/g),
				ending = $('.mm_pm_ending [data-field="ending"]').val().split(/\,\s*/),
				replace = {};

			$('.mm_pm_replace .mm_pm_group').each(function(){
				var key = $(this).find('input[data-field="replace-from"]').val(),
					val = $(this).find('input[data-field="replace-to"]').val();
				replace[key] = val;
			});

			return {'pages': pages, 'replace': replace, 'ending': ending};
		},

		prepareUrl: function(urlsArr, rpls, endsArr) {
			var postUrls = [];
			for (var i = 0, l = urlsArr.length; i < l; i++) {
				for (var key in rpls){ urlsArr[i] = urlsArr[i].replace(key, rpls[key]); }
				var url = urlsArr[i];
				if (url) postUrls.push(url);
				for (var j = 0, g = endsArr.length; j < g; j++) { if(url) postUrls.push(url+endsArr[j]); }
			}
			return postUrls;
		},

		addMask: function(arr, actionType) {
			var action = 'AddIncludedMask=Add Mask&',
				container = '#incl_masks',
				btn = '#AddIncludedMask',
				input = '#IncludedUrlMask_UrlMask';

			$('.mm_pm_progress').removeClass('mm_pm_progress_exclude');

			if (actionType === 'exclude') {
				action = 'AddExcludedMask=Add Mask&';
				container = '#excl_masks';
				btn = '#AddExcludedMask';
				input = '#ExcludedUrlMask_UrlMask';
				$('.mm_pm_progress').addClass('mm_pm_progress_exclude');
			}

			$('.mm_pm_progress > i').text('0%');
			$('.mm_pm_progress > span').css('width', '0%');
			setTimeout(function(){ $('.mm_pm_progress').show(); }, 500);

			var postUrl = $(btn).attr('data-post-url'),
				queue = arr.length,
				progress = 0;

			var i = 0,
				calcProgress = function(progs, queue) {

				},
				next = function() {
					if (i < arr.length) {
						var item = arr[i++];

						var addedMasks = [];
						$(container+' .mask-item-edit .mask-edit input').each(function(){
							addedMasks.push($(this).val());
						});

						if (addedMasks.indexOf(item) !== -1) {
							next();
							progress = 100/arr.length + progress;
							$('.mm_pm_progress > span').css('width', progress+'%');
							$('.mm_pm_progress > i').text(Math.round(progress)+'%');
							queue--;
							if (queue === 0) setTimeout(function(){ $('.mm_pm_progress').fadeOut(); }, 2000);
							return;
						}

						$(input).val(item);

						$.ajax({
							type: 'POST',
							url: postUrl,
							data: action + $("form[data-ajax=true]").serialize()+'&X-Requested-With=XMLHttpRequest',
							success: function(response){
								$(container).html(response);
								next();
							}
						});

						progress = 100/arr.length + progress;
						$('.mm_pm_progress > span').css('width', progress+'%');
						$('.mm_pm_progress > i').text(Math.round(progress)+'%');
						queue--;

						if (queue === 0) setTimeout(function(){ $('.mm_pm_progress').fadeOut(); }, 2000);
					}
				};
			next();
		},

		init: function() {
			if(pageMasks.isInitialized) return;
			pageMasks.renderUI();
			pageMasks.attachHandlers();
			pageMasks.isInitialized = true;
		}

	};

	//init
	pageMasks.init();

})();